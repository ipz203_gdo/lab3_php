<?php

namespace app\controllers;

use app\models\Continent;
use app\models\Country;
use app\models\UpdateCountry;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class CountryController extends Controller
{
    public function actionView($code = 'UA')
    {
        $country = Country::findOne(['code' => $code]);
        $continent = Continent::findOne(['continent_id' => $country['continent_id']]);

        $countryDataProvider = new ActiveDataProvider([
           'query' => Country::findOne(['code' => $code])
        ]);

        return $this->render('view', compact('country', 'continent', 'countryDataProvider'));
    }

    public function actionEdit($code = 'UA')
    {
        $country = Country::findOne(['code' => $code]);
        $continent = Continent::findOne(['continent_id' => $country['continent_id']]);

        $model = new UpdateCountry();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                Yii::$app->session->setFlash('success', 'Дані збережені');
                $country->continent_id = $model->continent_id;
                $country->name = $model->name;
                $country->official_name = $model->official_name;
                $country->code = $model->code;
                $country->area = $model->area;
                $country->currency = $model->currency;
                $country->capital = $model->capital;
                $country->save();
                return $this->refresh();
            }
            else {
                Yii::$app->session->setFlash('error', 'Помилка редагування');
            }
        }

        return $this->render('edit', compact('country', 'continent', 'model'));
    }
}