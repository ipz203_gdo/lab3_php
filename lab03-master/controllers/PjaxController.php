<?php

namespace app\controllers;

use app\models\City;
use app\models\CityLanguage;
use app\models\Continent;
use app\models\Country;
use app\models\Region;
use Yii;
use yii\web\Controller;
use app\models\WorldForm;


class PjaxController extends Controller
{
    public function actionWorldForm() {

        $model = new WorldForm();

        $continents = Continent::find()->asArray()->all();
        $countries = null;
        $regions = null;
        $cities = null;
        $continent = null;
        $country = null;
        $region = null;
        $city = null;

        if (Yii::$app->request->isAjax) {
            $model->load(Yii::$app->request->post());
            $continent = Continent::find()
                ->where(['continent_id' => $model->continent_id])
                ->one();
            $countries = Country::find()
                ->where(['continent_id' => $continent->continent_id])
                ->asArray()
                ->all();
            $country = Country::find()
                ->where(['country_id' => $model->country_id])
                ->one();
            $regions = Region::find()
                ->innerJoin('region')
                ->where(['country_id' => $country->country_id, 'language' => 'en'])
                ->all();
            $region = Region::find()
                ->where(['region_id' => $model->region_id])
                ->one();
            $cities = City::find()
                ->select(['city.*', 'city_language.name_language as city_name'])
                ->leftJoin('city_language', 'city_language.city_id = city.city_id')
                ->andWhere(['city.region_id' => $region->region_id])
                ->andWhere(['city_language.language' => 'en'])
                ->asArray()
                ->all();
            $city = CityLanguage::find()
                ->where(['city_id' => $model->city_id])
                ->andWhere(['language' => 'en'])
                ->asArray()
                ->all();
        }

        return $this->render('world-form', [
            'model' => $model,
            'continents' => $continents,
            'countries' => $countries,
            'regions' => $regions,
            'cities' => $cities,
            'continent' => $continent,
            'country' => $country,
            'region' => $region,
            'city' => $city,
        ]);
    }

}