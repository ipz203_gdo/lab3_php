<?php


use app\models\Continent;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Редагування ' . $country['name'];

$this->params['breadcrumbs'][] = ['label' => 'Continents', 'url' => ['continent/index']];
$this->params['breadcrumbs'][] = ['label' => $continent['name'], 'url' => ['continent/view', 'code' => $continent['code']]];
$this->params['breadcrumbs'][] = ['label' => $country['name'], 'url' => ['country/view', 'code' => $country['code']]];
$this->params['breadcrumbs'][] = ['label' => 'Edit' . ' ' . $country['name']];
?>

<div class="country-edit">
    <h1><?= Html::encode($this->title) ?></h1>

  <br>

    <?php $form = ActiveForm::begin([
        'id' => 'country-edit-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n{error}",
            'labelOptions' => ['class' => 'col-lg-2 col-form-label mr-lg-3'],
            'inputOptions' => ['class' => 'col-lg-6 form-control'],
            'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
        ]
    ]); ?>


    <?= $form->field($model, 'continent_id')->dropdownList(
        ArrayHelper::map(Continent::find()->asArray()->all(), 'continent_id', 'name'),
        ['options' => [$country['continent_id'] => ['Selected'=>true]]]
    ); ?>

    <?= $form->field($model, 'name')->textInput(['value' => $country['name']]) ?>
    <?= $form->field($model, 'official_name')->textInput(['value' => $country['official_name']]) ?>
    <?= $form->field($model, 'code')->textInput(['value' => $country['code']]) ?>
    <?= $form->field($model, 'area')->textInput(['value' => $country['area']]) ?>
    <?= $form->field($model, 'currency')->textInput(['value' => $country['currency']]) ?>
    <?= $form->field($model, 'capital')->textInput(['value' => $country['capital']]) ?>


    <div class="form-group">
        <div class="offset-lg-7 col-lg-5">
            <?= Html::submitButton('Зберегти', ['class' => 'btn btn-primary', 'name' => 'edit-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
