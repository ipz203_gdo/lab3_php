<?php

use yii\helpers\Html;
use yii\helpers\Url;


$this->title = $country->name;
$this->params['breadcrumbs'][] = ['label' => 'Continents', 'url' => ['continent/index']];
$this->params['breadcrumbs'][] = ['label' => $continent['name'], 'url' => ['continent/view', 'code' => $continent['code']]];
$this->params['breadcrumbs'][] = ['label' => $country['name'], 'url' => ['country/view', 'code' => $country['code']]];


$rep = array('[', ']');
$coords = str_replace($rep, "", $country['coords']);
$coords2 = explode(',', $coords);
$lat = $coords2[0];
$lnd = $coords2[1];
?>


<section id="continent-view">
  <div class="container">
    <div class="country">
      <h1><?= Html::encode($this->title) ?> <a href="<?= Url::to(['country/edit', 'code' => $country->code]); ?>" class="btn btn-sm btn-primary mx-3">Редагувати</a> </h1>
      <div class="wrapper d-flex justify-content-between">
        <div class="country__information col-6">
          <table class="table table-striped table-bordered">
            <tr>
              <th scope="row">Official Name</th>
              <td><?= $country['name'] ?></td>
            </tr>
            <tr>
              <th scope="row">Image</th>
              <td>
                  <?= Html::img('@web/images/countries/png100px/' . strtolower($country['code']) . '.png', ['alt' => $country['name']]) ?>
              </td>
            </tr>
            <tr>
              <th scope="row">Capital</th>
              <td><?= $country['capital'] ?></td>
            </tr>
            <tr>
              <th scope="row">Area</th>
              <td><?= $country['area'] ?></td>
            </tr>
            </tr>
            <tr>
              <th scope="row">Currency</th>
              <td><?= $country['currency'] ?></td>
            </tr>
          </table>

        </div>
        <div class="country__geolocation col-6">
          <script>
              async function initMap() {
                  let centerLatLng = new google.maps.LatLng(<?=$lat?>, <?=$lnd?>);
                  let point = new google.maps.LatLng(<?=$lat?>, <?=$lnd?>);
                  let mapOptions = {
                      center: centerLatLng,
                      zoom: 5
                  }

                  let map = new google.maps.Map(document.querySelector('.country__geolocation'), mapOptions);
                  // Создание метки на карте
                  let marker = new google.maps.Marker({
                      position: point,
                      map: map,
                      title: '<?=$country['name']?>',
                      draggable: false,
                      animation: google.maps.Animation.DROP
                  });
              }
              window.addEventListener('load', initMap)
          </script>
        </div>
      </div>
    </div>
  </div>
</section>
