<?php

use yii\helpers\Html;
use yii\helpers\Url;


$this->title = 'Continents';
$this->params['breadcrumbs'][] = ['label' => ''];

?>


<div class="list-group flex-row flex-wrap justify-content-center">
    <?php
    foreach ($continents as $continentItem): ?>
      <a href="<?= Url::to(['continent/view', 'code' => $continentItem['code']]); ?>" class="list-group-item continent col-lg-3 col-md-6 col-sm-12 m-3">
        <p class="card-header-continent"><?= Html::encode($continentItem['name']) ?></p>
        <?= Html::img('@web/images/continents/' . strtolower($continentItem['code']) . '.png', ['alt' => $continentItem['name']]) ?>
        <div class="card-footer">
          <span><?= Html::encode($continentItem['description']) ?></span>
        </div>
      </a>
    <?php endforeach; ?>
</div>
