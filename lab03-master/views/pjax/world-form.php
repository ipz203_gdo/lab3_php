<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
?>


<?php Pjax::begin([
    'id' => 'world-form-container',
    'options' => ['class' => 'd-flex justify-content-between' ]
]); ?>
<?php $form = ActiveForm::begin([
    'options' => ['data' => ['pjax' => true], 'class' => 'col-6'],
    'id' => 'world-form',
    'fieldConfig' => [
        'template' => "{label}\n{input}",
        'labelOptions' => ['class' => 'font-weight-bold col-form-label mr-lg-3'],
        'inputOptions' => ['class' => 'col-lg-12 form-control'],
    ]
]); ?>

<?= $form->field($model, 'continent_id')->dropDownList(ArrayHelper::map($continents, 'continent_id', 'name'), [
    'prompt' => '--Select a continent--',
    'id' => 'field-continent-id',
    'onchange' => '$("#world-form").submit()'
]) ?>

<?php if ($countries && $continent): ?>
  <?= $form->field($model, 'country_id')->dropDownList(ArrayHelper::map($countries, 'country_id', 'name'), [
      'prompt' => '--Select a country--',
      'id' => 'field-country-id',
      'onchange' => '$("#world-form").submit()'
  ]) ?>
<?php endif;?>

<?php if ($regions && ($model->country_id)): ?>
  <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($regions, 'region_id', 'name_language'), [
      'prompt' => '--Select a region--',
      'id' => 'field-region-id',
      'onchange' => '$("#world-form").submit()'
  ]) ?>
<?php endif;?>

<?php if ($cities && ($model->region_id)): ?>
  <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map($cities, 'city_id', 'city_name'), [
      'prompt' => '--Select a city--',
      'id' => 'field-city-id',
      'onchange' => '$("#world-form").submit()'
  ]) ?>
<?php endif;?>


<?php ActiveForm::end(); ?>
<table class="table table-bordered col-6">
  <tr style="height: 50px !important;">
    <th scope="row">Your place</th>
  </tr>
    <?php if ($continent): ?>
      <tr style="height: 50px !important;">
        <th scope="row">Continent</th>
        <td><?=$continent->name?></td>
      </tr>
    <?php endif; ?>

    <?php if ($country): ?>
      <tr style="height: 50px !important;">
        <th scope="row">Country</th>
        <td><?=$country->name?></td>
      </tr>
    <?php endif; ?>

    <?php if ($region): ?>
      <tr style="height: 50px !important;">
        <th scope="row">Region</th>
        <td><?=$region->name_language?></td>
      </tr>
    <?php endif; ?>

    <?php if ($city): ?>
      <tr style="height: 50px !important;">
        <th scope="row">City</th>
        <td><?=$city[0]['name_language']?></td>
      </tr>
      <caption class="font-weight-bold">Погода в місті <?=$city[0]['name_language']?></caption>
      <tr>
        <td></td>
      </tr>
    <?php endif; ?>
</table>


<?php Pjax::end(); ?>
