<?php

namespace app\models;

use yii\db\ActiveRecord;

class CityLanguage extends ActiveRecord
{
    public static function tableName()
    {
        return 'city_language';
    }

}