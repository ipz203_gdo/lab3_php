<?php

namespace app\models;

use yii\base\Model;

class UpdateCountry extends Model
{
    public $continent_id;
    public $name;
    public $official_name;
    public $code;
    public $area;
    public $currency;
    public $capital;

    public function rules()
    {
        return [
            [['continent_id', 'name', 'official_name', 'code', 'currency', 'capital'], 'required'],
            ['area', 'default', 'value' => 0],
            [['currency', 'capital'], 'trim'],
        ];
    }
}